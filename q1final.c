#include<stdio.h>
#include<stdlib.h>

long long int M1=1000000006;
long long int M2=1000000007;
long long int w[100008];
long long int u[100008];
long long int v[100008];
long long int tmp[100008];
//long long int flag[100008];
long long int unh[100008];
long long int flag;
long long int unhappy;

long long int  power6(long long  int  a,long long int  b)
{
	long long int  p=1;
	while(b>0)
	{
		if((b & 1) == 1)
		{
			p=((p%M1)*(a%M1))%M1;
		}

		a=((a)%M1*(a)%M1)%M1;
		b=b>>1;
	}
	return p%M1;
}


long long int  power7(long long  int  a,long long int  b)
{
	long long int  p=1;
	while(b>0)
	{
		if((b & 1 )== 1)
		{
			p=((p%M2)*(a%M2))%M2;
		}

		a=((a)%M2*(a)%M2)%M2;
		b=b>>1;
	}
	return p%M2;
}


long long int dmerge(long long int start,long long int mid,long long int end)
{
	long long int i;
	long long int warr1[100008];
	long long int warr2[100008];
	long long int uarr1[100008];
	long long int uarr2[100008];
	long long int varr1[100008];
	long long int varr2[100008];
	for(i=0;i<mid-start;i++)
	{
		warr1[i]=w[start+i];
		uarr1[i]=u[start+i];
		varr1[i]=v[start+i];
	}
	for(i=0;i<end-mid;i++)
	{
		warr2[i]=w[mid+i];
		uarr2[i]=u[mid+i];
		varr2[i]=v[mid+i];
	}
	long long int l=0;
	long long int r=0;
	long long int k=0;
	while(l<mid-start && r<end-mid)
	{
		if(warr1[l]<warr2[r])
		{
			w[k+start]=warr1[l];
			u[k+start]=uarr1[l];
			v[k+start]=varr1[l];
			l++;
			k++;
		}
		else if(warr1[l]>warr2[r])
		{
			w[k+start]=warr2[r];
			u[k+start]=uarr2[r];
			v[k+start]=varr2[r];
			r++;
			k++;
		}
		else
		{
			if(varr1[l]>varr2[r])
			{
				w[k+start]=warr1[l];
				u[k+start]=uarr1[l];
				v[k+start]=varr1[l];
				l++;
				k++;		
			}
			else
			{
				w[k+start]=warr2[r];
				u[k+start]=uarr2[r];
				v[k+start]=varr2[r];
				r++;
				k++;
			}
		}
	}
	while(l<mid-start)
	{
		w[k+start]=warr1[l];
		u[k+start]=uarr1[l];
		v[k+start]=varr1[l];
		l++;
		k++;
	}
	while(r<end-mid)
	{
		w[k+start]=warr2[r];
		u[k+start]=uarr2[r];
		v[k+start]=varr2[r];
		r++;
		k++;
	}
	return 0;
}
long long int dmergesort(long long int start,long long int end)
{
	if (end-start<=1)
	{
		return 0;
	}
	long long int mid;
	mid=(start+end)/2;
	dmergesort(start,mid);
	dmergesort(mid,end);
	dmerge(start,mid,end);
	return 0;
}


typedef struct Node
{
	long long int val;
	long long int wt;
	//	long long int p;
	struct Node *prev;
}node;

node *head[100008];
node *push(long long int x,long long int y,long long int z)
{
	node *top=head[y];
	node* tmp=(node*)malloc(sizeof(node));
	tmp->prev=top;
	tmp->val=x;
	tmp->wt=z;
	top=tmp;
	return top;
}

node* pop(node *top)
{
	if(top->prev!=NULL)
	{
		top=top->prev;
	}
	else
	{
		top=NULL;
	}

	return top;
}

long long int dfs(long long int parent,long long int i,long long pwt, long long int n)
{
	node *tmp;
	//	node *tmp1;
	if(flag==1)
	{
		return 0;
	}
	for(tmp=head[i];tmp!=NULL;tmp=tmp->prev)
	{
		if(flag==1)
		{
			return 0;
		}
		//		if(tmp->val!=ip)
		//		{
		//========================================================================================================		
		/*	if(tmp->prev!=NULL)
			{
			if(tmp->prev->wt==tmp->wt)
			unhappy=1;
			}*/
		//		if(unhappy==n)
		//		{
		//			break;
		//		}
		//=========================================================================================================
		if(tmp->val!=parent)
		{
			if(tmp->wt==pwt)
			{
				flag=1;
				return 0;
			}
			unh[tmp->val]=1;
			//unhappy++;

			//				printf("%lld \n",tmp->val );
			dfs(i,tmp->val,tmp->wt,n);
			//			prlong long intf("count[%lld]=%lld\n",i,count[i] );

		}
		//		}
		//			tmp1=tmp;
		//		tmp=tmp->prev;
		//		pop(tmp1);

	}

	return 0;
}

/*long long int dfs(long long int i)
  {
  node *tmp=head[i];
  while(tmp!=NULL)
  {
  if(flag[tmp->val]!=1)
  {
  if(unh[tmp->val]==0)
  unh[tmp->val]=1;
  prlong long intf("%lld ",tmp->val);
  flag[tmp->val]=1;
  dfs(tmp->val);
  }
  else
  return 0;
  tmp=tmp->prev;
  }
  return 0;
  }*/


int main()
{
	long long int n;
	scanf("%lld",&n);
	if(n==1)
		printf("1\n");
	else
	{
		long long int i;

		for(i=0;i<100008;i++)
			head[i]=NULL;


		//	long long int i;
		for (i = 0; i < n-1; i++)
		{
			scanf("%lld %lld %lld",&u[i],&v[i],&w[i]);
		}

		dmergesort(0,n-1);

		//	printf("\n");

		//	for(i=0;i<n-1;i++)
		//		printf("%lld %lld %lld\n",u[i],v[i],w[i] );

		for(i=0;i<n;i++)
			tmp[i]=u[i];
		for(i=0;i<n;i++)
			u[i+1]=tmp[i];

		for(i=0;i<n;i++)
			tmp[i]=v[i];
		for(i=0;i<n;i++)
			v[i+1]=tmp[i];

		for(i=0;i<n;i++)
			tmp[i]=w[i];
		for(i=0;i<n;i++)
			w[i+1]=tmp[i];


		u[0]=0;
		v[0]=0;
		w[0]=0;

		//	printf("\n");
		//	printf("\n");

		//	for(i=1;i<=n-1;i++)
		//	{
		//		printf("%lld %lld %lld\n",u[i],v[i],w[i] );
		//	}

		for(i=1;i<=n-1;i++)
		{
			head[u[i]]=push(v[i],u[i],w[i]);
			head[v[i]]=push(u[i],v[i],w[i]);
		}

		/*	for(i=0;i<100008;i++)
			count[i]=1;*/ 
		//	scanf("%lld",&ip);
		/*	for(i=0;i<e;i++)
			{
			scanf("%lld %lld",&u,&v);
			head[u]=push(v,u);		//v-u
			head[v]=push(u,v);		//u-v
			}*/

		//	printf("inserted\n");


		/*	for(i=1;i<=n;i++)
			{
		//		printf("head[%lld]->",i);
		node *tmp=head[i];
		while(tmp!=NULL)
		{

		//			printf("%lld:%lld::",tmp->val,tmp->wt);
		tmp=tmp->prev;
		}
		//		printf("\n");
		}

		printf("\n");*/
		long long int ip;
		for(i=1;i<=n;i++)
		{
			ip=i;
			//		prlong long intf("head[%lld]->",i);
			node *tmp;
			for(tmp=head[i];tmp->prev!=NULL;tmp=tmp->prev)
			{
				if(tmp->wt==tmp->prev->wt)
				{	
					//		flag[tmp->val]=1;
					//				printf("%lld %lld\n",tmp->val,tmp->prev->val);
					//			if(unhappy==n)
					//				break;
					unh[tmp->val]=1;
					//				unhappy++;
					//				printf("unh[%lld]=%lld\n",tmp->val,unh[tmp->val] );
					dfs(ip,tmp->val,tmp->wt,n);
					//		flag[tmp->prev->val]=1;
					//			if(unhappy==n)
					//				break;
					unh[tmp->prev->val]=1;
					//				unhappy++;
					dfs(ip,tmp->prev->val,tmp->prev->wt,n);
				}
				//		prlong long intf("%lld:%lld:%lld:",tmp->val,tmp->wt,tmp->p);
				//		tmp=tmp->prev;
			}
			//	printf("\n");
		}



		//	flag[ip]=1;
		//	dfs(1);
		unh[0]=-1;
		//	for(i=1;i<=n;i++)
		//		printf("%lld ",unh[i]);
		//	printf("\n");
		long long int sum=0;
		if(flag==1)
		{
			printf("0\n");
		}
		else	
		{
			for(i=1;i<=n;i++)
			{
				if(unh[i]==0)
					sum++;
			}

			long long int isum=power6(sum,sum);
			long long int ans=power7(sum,isum);

			//if(unhappy==1)
			//	printf("0\n");
			//else
			printf("%lld\n",ans);
		}
	}
	return 0;
}
