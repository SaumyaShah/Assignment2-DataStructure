#include<stdio.h>
#include<stdlib.h>

long long int flag[1008];
long long int x[1008];
long long int y[1008];
long long int r[1008];
char dir[1008];

typedef struct Node
{
	long long int val;
	struct Node *prev;
}node;

long long int gcd(long long int a,long long int b)
{
//	long long int fl=0;
/*	if(a==b)
		return 1;*/
	while(a!=b)
	{
		if(a>b)
			a=a-b;
		else 
			b=b-a;
	}
	
	return a;
}

node *head[1008];
node *push(long long int x,long long int y)
{
		node *top=head[y];
        node* tmp=(node*)malloc(sizeof(node));
        tmp->prev=top;
        tmp->val=x;
        top=tmp;
        return top;
}

/*node* pop(node *top)
{
	if(top->prev!=NULL)
	{
		top=top->prev;
	}
	else
			top=NULL;
	}

	return top;
}*/

long long int chk;
long long int dfs(long long int parent,long long int n)
{
//	printf("chk=%lld\n",chk );
/*	if(parent==n)
	{
		chk=1;
			return 0;
	}*/
	node *tmp;
	for(tmp=head[parent];tmp!=NULL;tmp=tmp->prev)
	{
		if(dir[tmp->val]!= dir[parent] && flag[tmp->val] != 1)
		{
//			printf("in if %lld \n",tmp->val );
			flag[tmp->val]=1;
			if(dir[parent]=='c')
				dir[tmp->val]='a';
			else if(dir[parent]!='c')
				dir[tmp->val]='c';
			dfs(tmp->val,n);
		}
		else if(dir[tmp->val]==dir[parent] && flag[tmp->val]==1)
		{
//				printf("in else %lld \n",tmp->val );
				chk=-1;
				break;
		}
	}
	return 0;
}

int main()
{
	long long int n;
	scanf("%lld",&n);
	long long int i;
	for(i=0;i<1008;i++)
		dir[i]='.';
	for(i=1;i<=n;i++)
		scanf("%lld %lld %lld",&x[i],&y[i],&r[i]);
	long long int j;

	for(i=1;i<=n;i++)
	{
		for(j=1;j<=n;j++)
		{
			if((x[i]-x[j])*(x[i]-x[j]) + (y[i]-y[j])*(y[i]-y[j])==(r[i]+r[j])*(r[i]+r[j]))
			{
				head[i]=push(j,i);		//v-u
//				head[j]=push(i,j);		//u-v
			}
		}
	}

//	printf("inserted\n");

/*	for(i=1;i<=n;i++)
	{
		printf("head[%lld]->",i);
		node *tmp=head[i];
		while(tmp!=NULL)
		{
			printf("%lld ",tmp->val);
			tmp=tmp->prev;
		}
		printf("\n");
	}*/
	
	flag[1]=1;
	dir[1]='c';

	dfs(1,n);
//	printf("%lld %lld\n",r[1],r[n] );
	long long int g=gcd(r[1],r[n]);
//	printf("gcd=%lld\n",g );

/*	printf("final flag arr:\n");
	for (long long int i = 0; i < n; ++i)
	{
		printf("%lld\n",flag[i] );
		
	}*/

	/*	printf("final dir arr:\n");
		for (long long int i = 0; i < n; ++i)
		{
			printf("%c\n",dir[i] );
			
		}*/

/*	printf("flag:\n");
	for(i=0;i<=n;i++)
	{
		printf("%lld ",flag[i] );
	}
	printf("\n");
	for(i=0;i<=n;i++)
		printf("%c ",dir[i] );
	printf("\n");*/

	if(flag[n]==0)
		printf("The input gear is not connected to the output gear.\n");
	else if(chk==-1)
		printf("The input gear cannot move.\n");
	else if(dir[1]==dir[n] && flag[n]==1)
		printf("%lld:%lld\n",r[1]/g,r[n]/g );
	else if(dir[1]!=dir[n] && flag[n]==1)
		printf("%lld:%lld\n",-r[1]/g,r[n]/g );

	return 0;
}

