#include<stdio.h>
#include<stdlib.h>
long long int M=1000000007;
long long int color[100008];
long long int flag[100008];
//long long int weight[100008];
typedef struct Node
{
	long long int val;
	struct Node *prev;
	long long int r;
	long long int b;
	long long int w;
}node;

node *head[100008];
node *push(long long int x,long long int y,long long int z)
{
		node *top=head[y];
        node* tmp=(node*)malloc(sizeof(node));
        tmp->prev=top;
        tmp->val=x;
        tmp->w=z;
        tmp->b=0;
        tmp->r=0;
        top=tmp;
        return top;
}

node* pop(node *top)
{
	if(top->prev!=NULL)
	{
		top=top->prev;
	}
	else
	{
		top=NULL;
	}

	return top;
}


long long int rstore(long long int parent,long long int n)
{
//	printf("in store parent=%lld\n",parent);
//	printf("r val\n");
	long long int i;
/*	for(i=1;i<=n;i++)
		printf("head[%lld]->r=%lld\n",i,head[i]->r);*/

	node *tmp;
	for(tmp=head[parent];tmp!=NULL;tmp=tmp->prev)
	{
		if(flag[tmp->val]!=1)
		{
			flag[tmp->val]=1;
//			printf("%lld ",tmp->val );
//			printf("calling store(parent) in if \n");
			head[parent]->r=((head[parent]->r)%M+(rstore(tmp->val,n))%M)%M;
//			printf("head[%lld]->r==%lld\n",parent,head[parent]->r );			
		}
	}
//	printf("return from fun parent=%lld\n",parent);
	if(color[parent]==0)
	{
		head[parent]->r=((head[parent]->r)%M+1)%M;
		return head[parent]->r;
	}
	else
	{
		head[parent]->r+=0;
		return head[parent]->r;
	}
}


long long int bstore(long long int parent,long long int n)
{
//	printf("in store parent=%lld\n",parent);
//	printf("b val\n");
	long long int i;
/*	for(i=1;i<=n;i++)
		printf("head[%lld]->b=%lld\n",i,head[i]->b);*/

	node *tmp;
	for(tmp=head[parent];tmp!=NULL;tmp=tmp->prev)
	{
		if(flag[tmp->val]!=1)
		{
			flag[tmp->val]=1;
//			printf("%lld ",tmp->val );
//			printf("calling store(parent) in if \n");
			head[parent]->b=((head[parent]->b)%M+(bstore(tmp->val,n))%M)%M;
//			printf("head[%lld]->b==%lld\n",parent,head[parent]->b );			
		}
	}
//	printf("return from fun parent=%lld\n",parent);
	if(color[parent]==1)
	{

		head[parent]->b=((head[parent]->b)%M+1)%M;
//		printf("return in if head[%lld]->b=%lld\n",parent,head[parent]->b);
		return head[parent]->b;
	}
	else
	{
		head[parent]->b+=0;
//		printf("return in else head[%lld]->b=%lld\n",parent,head[parent]->b);
		return head[parent]->b;
	}
}

long long int bweight;
long long int bdfs(long long int parent,long long int black)
{
//	printf("in dfs\n");
	node *tmp;
	for(tmp=head[parent];tmp!=NULL;tmp=tmp->prev)
	{
		if(flag[tmp->val]!=1)
		{
			flag[tmp->val]=1;
			bweight=((bweight%M)+((head[tmp->val]->b)%M)*((black-head[tmp->val]->b)%M)*((tmp->w)%M))%M;
//			printf("%lld %lld",tmp->val,bweight );	
			bdfs(tmp->val,black);
		}
	}
	return 0;
}

long long int rweight;
long long int rdfs(long long int parent,long long int red)
{
//	printf("in rdfs\n");
	node *tmp;
	for(tmp=head[parent];tmp!=NULL;tmp=tmp->prev)
	{
		if(flag[tmp->val]!=1)
		{
			flag[tmp->val]=1;
			rweight=(rweight%M+(((head[tmp->val]->r)%M)*((red-head[tmp->val]->r)%M)*((tmp->w)%M))%M)%M;
		//	printf("%lld %lld",tmp->val,rweight );	
			rdfs(tmp->val,red);
		}
	}
	return 0;
}
/*long long int dfs(long long int parent,long long int n)
{

	node *tmp;
	for(tmp=head[parent];tmp!=NULL;tmp=tmp->prev)
	{
		if(flag[tmp->val]!=1)
		{
			flag[tmp->val]=1;
		
			printf("%lld ",tmp->val );	
			dfs(tmp->val,n);
		}
	}
	return 0;
}*/



int main()
{
	long long int n,u,v,ew;
	scanf("%lld",&n);

	long long int i;

	for(i=0;i<100008;i++)
		head[i]=NULL;

	for(i=1;i<=n;i++)
		scanf("%lld",&color[i]);
//	printf("%lld\n",color[1] );
	long long int chk=0;
	if(n>1)
	{
		for(i=1;i<=n-1;i++)
		{
			scanf("%lld %lld %lld",&u,&v,&ew);
			head[u]=push(v,u,ew);		//v-u
			head[v]=push(u,v,ew);		//u-v
//		weight[v]=ew;
		}


//	printf("inserted\n");

/*	for (i = 1; i <=n; i++)
	{
		printf("head[%lld]->w=%lld\n",i,head[i]->w );
	}*/

	flag[1]=1;
	head[1]->b=bstore(1,n);
//	printf("in main head[1]->b=%lld\n",head[1]->b );
	for(i=0;i<100008;i++)
		flag[i]=0;
	flag[1]=1;
	head[1]->r=rstore(1,n);
/*	printf("final b values:\n");
	for (i = 1; i <=n; ++i)
	{
		printf("head[%lld]->b=%lld\n",i,head[i]->b);
	}

	printf("final r values:\n");
	for (i = 1; i <=n; ++i)
	{
		printf("head[%lld]->r=%lld\n",i,head[i]->r);
	}*/
	long long int black=0;
	long long int red=0;
	for(i=1;i<=n;i++)
	{
		if(color[i]==1)
			black++;
		else
			red++;
	}
//	printf("ew value:\n");
/*	for (i = 1; i <=n; i++)
	{
		printf("head[%lld]->w=%lld\n",i,head[i]->w );
	}*/
//	long long int bweight=0;
//	long long int rweight=0;

	for(i=0;i<100008;i++)
		flag[i]=0;

	bdfs(1,black);

	for(i=0;i<100008;i++)
		flag[i]=0;

	rdfs(1,red);

/*	for(i=2;i<=n;i++)
	{
		printf("head[i]->b=%lld black-head[i]->b=%lld head[i]->w=%lld\n",head[i]->b,black-head[i]->b,head[i]->w );
		bweight+=(head[i]->b)*(black-head[i]->b)*(head[i]->w);
		printf("bweight=%lld\n",bweight);
	}

	for(i=2;i<=n;i++)
	{
		rweight+=(head[i]->r)*(red-head[i]->r)*(head[i]->w);
		printf("rweight=%lld\n",rweight );
	}*/

//	printf("bweight=%lld\n",bweight );
//	printf("rweight=%lld\n",rweight );

	long long int tweight=(rweight%M+bweight%M)%M;
	printf("%lld\n",tweight%M);
}
/*	for(i=1;i<=n;i++)
	{
		printf("head[%lld]->\n",i);
		node *tmp=head[i];
		while(tmp!=NULL)
		{
			printf("%lld %lld %lld\n",tmp->val,tmp->w,tmp->b);
			tmp=tmp->prev;
		}
		printf("\n");
	}*/
else
	printf("0\n");
	

//	dfs(0,0);

/*	for(i=0;i<n;i++)
		printf("%lld ",count[i]);
	printf("\n");*/

	return 0;
}
