#include<stdio.h>
long long int dd[100008];
long long int td[100008];
long long int sd[100008];
long long int heap[100008];
long long int hday[100008];
long long int dmerge(long long int start,long long int mid,long long int end)
{
	long long int i;
	long long int ddarr1[100008];
	long long int ddarr2[100008];
	long long int tdarr1[100008];
	long long int tdarr2[100008];
	long long int sdarr1[100008];
	long long int sdarr2[100008];
	for(i=0;i<mid-start;i++)
	{
		ddarr1[i]=dd[start+i];
		tdarr1[i]=td[start+i];
		sdarr1[i]=sd[start+i];
	}
	for(i=0;i<end-mid;i++)
	{
		ddarr2[i]=dd[mid+i];
		tdarr2[i]=td[mid+i];
		sdarr2[i]=sd[mid+i];
	}
	long long int l=0;
	long long int r=0;
	long long int k=0;
	while(l<mid-start && r<end-mid)
	{
		if(ddarr1[l]<ddarr2[r])
		{
			dd[k+start]=ddarr1[l];
			td[k+start]=tdarr1[l];
			sd[k+start]=sdarr1[l];
			l++;
			k++;
		}
		else if(ddarr1[l]>ddarr2[r])
		{
			dd[k+start]=ddarr2[r];
			td[k+start]=tdarr2[r];
			sd[k+start]=sdarr2[r];
			r++;
			k++;
		}
		else
		{
			if(sdarr1[l]>sdarr2[r])
			{
				dd[k+start]=ddarr1[l];
				td[k+start]=tdarr1[l];
				sd[k+start]=sdarr1[l];
				l++;
				k++;		
			}
			else
			{
				dd[k+start]=ddarr2[r];
				td[k+start]=tdarr2[r];
				sd[k+start]=sdarr2[r];
				r++;
				k++;
			}
		}
	}
	while(l<mid-start)
	{
		dd[k+start]=ddarr1[l];
		td[k+start]=tdarr1[l];
		sd[k+start]=sdarr1[l];
		l++;
		k++;
	}
	while(r<end-mid)
	{
		dd[k+start]=ddarr2[r];
		td[k+start]=tdarr2[r];
		sd[k+start]=sdarr2[r];
		r++;
		k++;
	}
	return 0;
}
long long int dmergesort(long long int start,long long int end)
{
	if (end-start<=1)
	{
		return 0;
	}
	long long int mid;
	mid=(start+end)/2;
	dmergesort(start,mid);
	dmergesort(mid,end);
	dmerge(start,mid,end);
	return 0;
}
long long int delete(long long int n)
{
	if(n==0)
		return 0;
	long long int tmp1;
	tmp1=heap[0];
	heap[0]=heap[n-1];
	heap[n-1]=tmp1;
	long long int tmp2;
	tmp2=hday[0];
	hday[0]=hday[n-1];
	hday[n-1]=tmp2;
	heap[n-1]=0;
	hday[n-1]=0;
	long long int p=0;
	long long int pnext;
	while(p < n/2) 
	{
		if(heap[p]<heap[2*p+1] || heap[p]<heap[2*p+2])
		{
			if(heap[2*p+1]>heap[2*p+2])
			{
				tmp1=heap[2*p+1];
				heap[2*p+1]=heap[p];
				heap[p]=tmp1;
				tmp2=hday[2*p+1];
				hday[2*p+1]=hday[p];
				hday[p]=tmp2;
				pnext=2*p+1;
			}
			else
			{
				tmp1=heap[2*p+2];
				heap[2*p+2]=heap[p];
				heap[p]=tmp1;
				tmp2=hday[2*p+2];
				hday[2*p+2]=hday[p];
				hday[p]=tmp2;
				pnext=2*p+2;
			}
			p=pnext;
		}
		else
			break;
	}
	n--;
	return n;
}
long long int max()
{
	return heap[0];
}
long long int insert(long long int p,long long int n)
{
	long long int tmp1,tmp2;
	while(p!=0)
	{
		if(heap[p]>heap[(p-1)/2])
		{
			tmp1=heap[p];
			heap[p]=heap[(p-1)/2];
			heap[(p-1)/2]=tmp1;
			tmp2=hday[p];
			hday[p]=hday[(p-1)/2];
			hday[(p-1)/2]=tmp2;
		}
		else
			break;
		p=(p-1)/2;
	}
	n++;
	return n;
}
long long int sub(long long int n)
{
	hday[0]--;
	if(hday[0]==0)
	{
		n=delete(n);
	}
	return n;
}
int main()
{
	long long int T;
	scanf("%lld",&T);
	while(T--)
	{
		long long int n;
		long long int day;
		scanf("%lld %lld",&n,&day);
		long long int cday=day;
		long long int size=0;
		long long int i;
		for (i = 0; i < n; i++)
		{
			scanf("%lld %lld %lld",&dd[i],&td[i],&sd[i]);
		}
		dmergesort(0,n);
		long long int tmpday;
		i=0;
		for(tmpday=1;cday>0;)
		{
			if(dd[i]==tmpday)
			{
				while(i<n)
				{
					if(dd[i]==dd[i+1])
					{
						heap[i]=sd[i];
						hday[i]=td[i];
						size=insert(i,size);
					}
					else
					{
						heap[i]=sd[i];
						hday[i]=td[i];
						size=insert(i,size);
						break;
					}	
					i++;
				}
				if(sd[i]==max() && hday[0]!=0)
				{
					hday[0]--;
					if(hday[0]==0)
					{
						size=delete(size);
					}
				}
				else if(hday[0]!=0)
				{
					size=sub(size);
				}
				if(i<n)
				{
					i++;	
				}
			}
			else if(hday[0]!=0)
			{
				size=sub(size);
			}
			tmpday++;
			cday--;
		}
		long long int sum=0;
		for (i = 0; i < n; i++)
		{
			sum+=heap[i]*hday[i];
		}
		printf("%lld\n",sum);
		for(i=0;i<n;i++)
		{
			sd[i]=0;
			td[i]=0;
			dd[i]=0;
			heap[i]=0;
			hday[i]=0;
		}
	}
	return 0;
}
